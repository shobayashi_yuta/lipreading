############################
# 第５回サイレント音声認識ワークショップ「機械読唇チャレンジ」用プログラム
############################

import argparse
import glob
import os
import csv
import numpy as np
import copy
from PIL import Image
import six

import chainer
import chainer.functions as F
from chainer import function
import chainer.links as L
from chainer import link
from chainer.training import extensions
from chainer.training import extension
from chainer.dataset import iterator as iterator_module
from chainer import training
from chainer.links.caffe import CaffeFunction
from chainer import Variable, optimizers, Chain, cuda
from chainer.functions.evaluation import accuracy
from chainer.functions.loss import softmax_cross_entropy
from chainer import reporter as reporter_module
from chainer import configuration

import matplotlib
matplotlib.use('Agg')


import alexLike
#import compressed_pickle as pickle
import joblib
from multiprocessing import Pool

def main():
    
    parser = argparse.ArgumentParser()
    parser.add_argument('--corpus', '-c', type=str, default='/mnt/net/corpus/SSSD',
                        help='読み込むコーパス')
    parser.add_argument('--gpu', '-g', type=int, default=-1,
                        help='GPU ID (negative value indicates CPU)')
    parser.add_argument('--epoch', '-e', type=int, default=5,
                        help='Number of sweeps over the dataset to train')
    parser.add_argument('--out', '-o', default='result',
                        help='Directory to output the result')
    parser.add_argument('--save', action='store_true',
                        help='save training model')
    args = parser.parse_args()
    data_type = 'LF-ROI' # 画像を直接入れるかランドマークを利用するか
    global dataPath
    dataPath = args.corpus + "/" + data_type + "/"
    training_path = args.corpus + "/exp_set_PRMU201803/training_LF-ROI.txt"
    val_path = args.corpus + "/exp_set_PRMU201803/validation_LF-ROI.txt"
    global dirpath
    #dirpath = "/mnt/data/shobayashi/cache/"
    dirpath = "cacheG80/"
    train = read_dataset(training_path)
    #val = train
    val = read_dataset(val_path)
    #pickArrays(train[0][0])
    train_iter = chainer.iterators.SerialIterator(train, batch_size=25*2, repeat=True, shuffle=True)
    val_iter = chainer.iterators.SerialIterator(val, batch_size=1, repeat=False, shuffle=False)
    
    #print([[t,args.gpu] for t in train])
    #wrapperA([[t] for t in train][0])
    '''
    p = Pool(processes = 8)
    p.map(wrapper,[[t] for t in train])
    p.close()
    p = Pool(processes = 8)
    p.map(wrapper,[[t] for t in val])
    p.close()
    import sys;sys.exit()
    '''

    #読み込むcaffeモデルとpklファイルを保存するパス
    #loadpath = "bvlc_alexnet.caffemodel"
    #alexnet = CaffeFunction(loadpath)
    rnn = RNN()
    model = Classifier(rnn)
    #alexLike.copy_model(alexnet, model)
    if args.gpu >= 0:
        # Make a specified GPU current
        chainer.backends.cuda.get_device_from_id(args.gpu).use()
        model.to_gpu()
    #optimizer = chainer.optimizers.MomentumSGD(lr=0.01,momentum=0.9)
    #optimizer = chainer.optimizers.SGD(lr=0.001)
    optimizer = chainer.optimizers.Adam(alpha=0.0001, beta1=0.9, beta2=0.999, eps=1e-08)
    #optimizer = chainer.optimizers.Adam()
    
    optimizer.setup(model)
    #optimizer.add_hook(chainer.optimizer.Lasso(0.001))
    optimizer.add_hook(chainer.optimizer.WeightDecay(0.0005))
   
    #model.predictor.base.disable_update()
    #global updater
    updater = S3DUpdater(train_iter, optimizer, args.gpu)
     
    trainer = training.Trainer(updater, (args.epoch, 'epoch'), out=args.out)
    
    #eval_model = model.copy()  # Model with shared params and distinct states
    #eval_rnn = eval_model.predictor
    trainer.extend(lipEvaluator(
        val_iter, model,device=args.gpu
        # Reset the RNN state at the beginning of each evaluation
        #eval_hook=lambda _: eval_rnn.reset_state()
        ))
    
    interval = 1
    trainer.extend(extensions.LogReport(trigger=(interval, 'epoch')))
    trainer.extend(extensions.PrintReport(
        ['epoch', 'main/loss', 'validation/main/loss',
         'main/accuracy', 'validation/main/accuracy','elapsed_time']
    ), trigger=(interval, 'iteration'))

    trainer.extend(
    extensions.PlotReport(['main/loss','validation/main/loss'],
                          'epoch', file_name='loss.png'))
    trainer.extend(extensions.ProgressBar(
        update_interval=1))
    
    #trainer.extend(extensions.snapshot())
    trainer.extend(extensions.snapshot(filename='snapshot_epoch-{.updater.epoch}'))
    trainer.run()
    if args.save:
        from chainer import serializers
        serializers.save_npz('my.model', model)

    return 0

def wrapper(args):
    return makepickle(*args)

def wrapperA(args):
    return makepickleArrays(*args)

class RNN(chainer.Chain):
    
    def __init__(self):
        output_size = 25
        hidden_size = 256
        self.use_dropout = 0.5
        n_layers = 2
        super(RNN, self).__init__()
        
        #chainermodel = './VGG_ILSVRC_16_layers.npz'
        with self.init_scope():
            
            self.conv3_1 = L.ConvolutionND(ndim=3, in_channels=1, out_channels=32, ksize=(3,5,5), pad=1,stride=(1,2,2),nobias=True)
            self.conv3_2 = L.ConvolutionND(ndim=3, in_channels=32, out_channels=64, ksize=(3,5,5), pad=1,stride=(1,1,1),nobias=True)
            self.conv3_3 = L.ConvolutionND(ndim=3, in_channels=64, out_channels=96, ksize=(3,3,3), pad=1,stride=(1,1,1),nobias=True)
            #self.conv3_11 = L.ConvolutionND(ndim=3, in_channels=32, out_channels=32, ksize=(3,5,5), pad=1,stride=(1,2,2),nobias=True)
            #self.conv3_22 = L.ConvolutionND(ndim=3, in_channels=64, out_channels=64, ksize=(3,5,5), pad=1,stride=(1,1,1),nobias=True)
            #self.conv3_33 = L.ConvolutionND(ndim=3, in_channels=96, out_channels=96, ksize=(3,3,3), pad=1,stride=(1,1,1),nobias=True)

            #self.conv3_4 = L.ConvolutionND(ndim=3, in_channels=96, out_channels=1, ksize=(3,3,3), pad=1,stride=(1,1,1),nobias=True)
            self.bn1 = L.BatchNormalization(32)
            self.bn2 = L.BatchNormalization(64)
            self.bn3 = L.BatchNormalization(96)
            self.bn4 = L.BatchNormalization(2400)
            self.fc1 = L.Linear(None, hidden_size)
            
            

            
            self.lstm=L.NStepBiGRU(n_layers=n_layers, in_size=2400,out_size=hidden_size, dropout=self.use_dropout)
            self.bn5 = L.BatchNormalization(hidden_size*2)
            self.bn6 = L.BatchNormalization(hidden_size*2)
            self.fc2 = L.Linear(None, hidden_size*2)
            self.fc3 = L.Linear(None, hidden_size*2)
            self.fc4 = L.Linear(None, hidden_size*2)
            self.fc5 = L.Linear(None, hidden_size*2)
            self.fc6 = L.Linear(None, hidden_size*2)
            self.fc7 = L.Linear(None, hidden_size*2)
            
            self.fc8 = L.Linear(None, output_size)

            self.bnCSV = L.BatchNormalization(1)
            #self.bn111 = L.BatchNormalization(1)
    
    def call_imagesNN(self,xs):
        #xs = F.transpose(xs,(1,0,2,3))
        
        xs = F.expand_dims(xs,axis=0)
            
        xs = F.expand_dims(xs,axis=0)
        
        h2 = self.conv3_1(xs)
        h2 = self.bn1(h2)
        #h2 = self.conv3_11(h2)
        #h2 = self.bn1(h2)
        h2 = F.relu(h2)          
        h2 = F.dropout(h2, ratio=0.5) 
        h2 = F.max_pooling_nd(h2,ksize=(1,2,2),stride=(1,2,2))
    
        h2 = self.conv3_2(h2)
        h2 = self.bn2(h2)
        #h2 = self.conv3_22(h2)
        #h2 = self.bn2(h2)
        h2 = F.relu(h2)            
        h2 = F.dropout(h2, ratio=0.5) 
        h2 = F.max_pooling_nd(h2,ksize=(1,2,2),stride=(1,2,2))
            
        h2 = self.conv3_3(h2)
        h2 = self.bn3(h2)
        #h2 = self.conv3_33(h2)
        #h2 = self.bn3(h2)
        h2 = F.relu(h2)            
        h2 = F.dropout(h2, ratio=0.5) 
        h2 = F.max_pooling_nd(h2,ksize=(1,2,2),stride=(1,2,2))
            
        #import ipdb;ipdb.set_trace()
        h2 = F.transpose(h2,(0,2,1,3,4))[0]
        #import ipdb;ipdb.set_trace()
        #import ipdb;ipdb.set_trace()
        #h2 = self.fc1(h2)
        h2 = F.reshape(h2,(len(h2),-1))
        #h2 = self.bn4(h2)
        #h2 = F.relu(h2)
        #h2 = F.dropout(h2, ratio=0.5)
        return h2
    '''
    def call_csvNN(self,x):
        return self.bnCSV(x)
    '''
    def __call__(self, x):
        """
        Parameters
        xs : list(Variable)
        """
        #self.length = [len(x) for x in xs]
        #yy = []
        yy = [self.call_imagesNN(xs) for xs in x]
        
        yy = [ self.bnCSV(F.expand_dims(F.expand_dims(xs,axis=0),axis=0))[0][0] for xs in yy]
        hy, _= self.lstm(None, yy) 
        #hy, _,_ = self.lstm(None,None, x) 
        #import ipdb;ipdb.set_trace()
        #import ipdb;ipdb.set_trace()
        yy = F.concat(hy[2:])
        #yy = hy[0]
        '''
        yy = F.relu(self.bn6(yy))
        yy = F.dropout(yy, ratio=0.5)
        
        
        
        yy = self.fc2(yy)
        yy = self.bn5(yy)
        yy = F.relu(yy)   
        yy = F.dropout(yy, ratio=0.5)
        
        
        yy = self.fc3(yy)
        yy = self.bn5(yy)
        yy = F.relu(yy)   
        yy = F.dropout(yy, ratio=0.5)

        yy = self.fc4(yy)
        yy = self.bn5(yy)
        yy = F.relu(yy)   
        yy = F.dropout(yy, ratio=0.5)
        
        yy = self.fc5(yy)
        yy = self.bn5(yy)
        yy = F.relu(yy)   
        yy = F.dropout(yy, ratio=0.5)
        
        yy = self.fc6(yy)
        yy = self.bn5(yy)
        yy = F.relu(yy)   
        yy = F.dropout(yy, ratio=0.5)

        yy = self.fc7(yy)
        yy = self.bn5(yy)
        yy = F.relu(yy)   
        yy = F.dropout(yy, ratio=0.5)
        ''' 

        yy = self.fc8(yy)
        
        #yy = 1
        
        return yy

    def splitInput(self,x,length):
        #import ipdb;ipdb.set_trace()
        #x = F.separate(x,axis=0)
        xlist = []
        init = 0
        for l in length:
            temp  = x[init:init+l]
            xlist.append(temp)
            init += l
        return xlist



class Classifier(link.Chain):
    compute_accuracy = True

    def __init__(self, predictor,
                 lossfun=softmax_cross_entropy.softmax_cross_entropy,
                 accfun=accuracy.accuracy,
                 label_key=-1):
        if not (isinstance(label_key, (int, str))):
            raise TypeError('label_key must be int or str, but is %s' %
                            type(label_key))

        super(Classifier, self).__init__()
        self.lossfun = lossfun
        #self.lossfun = lossfun(class_weight=cupy.ndarray([1,2]).astype(cupy.int32))
        self.accfun = accfun
        self.y = None
        self.loss = None
        self.accuracy = None
        self.label_key = label_key

        with self.init_scope():
            self.predictor = predictor

    def __call__(self, *args, **kwargs):

        if isinstance(self.label_key, int):
            if not (-len(args) <= self.label_key < len(args)):
                msg = 'Label key %d is of bounds' % self.label_key
                raise ValueError(msg)
            t = args[self.label_key]
            if self.label_key == -1:
                args = args[:-1]
            else:
                args = args[:self.label_key] + args[self.label_key + 1:]
        elif isinstance(self.label_key, str):
            if self.label_key not in kwargs:
                msg = 'Label key "%s" is not found' % self.label_key
                raise ValueError(msg)
            t = kwargs[self.label_key]
            del kwargs[self.label_key]

        self.y = None
        self.loss = None
        self.accuracy = None
        
        self.y = self.predictor(*args, **kwargs)
        #import ipdb;ipdb.set_trace()
        #print(len(self.y),len(t))
        #print(len(self.y),len(t))
        #print(self.y[0])
        #sys.exit()
        #import ipdb;ipdb.set_trace()
        self.loss = self.lossfun(self.y, t)
        #ave = sum(self.predictor.length)/len(self.predictor.length)
        #ave = sum(self.predictor.length)
        #import ipdb;ipdb.set_trace()
        if self.compute_accuracy:
            self.accuracy = self.accfun(self.y, t)
            reporter_module.report({'accuracy': self.accuracy}, self)
        reporter_module.report({'loss': self.loss}, self)
        
        return self.loss

class S3DUpdater(training.updaters.StandardUpdater):

    def __init__(self, train_iter, optimizer, device):
        super(S3DUpdater, self).__init__(
            train_iter, optimizer, device=device)

    # The core part of the update routine can be customized by overriding.
    def update_core(self):
        loss = 0
        # When we pass one iterator and optimizer to StandardUpdater.__init__,
        # they are automatically named 'main'.
        train_iter = self.get_iterator('main')
        optimizer = self.get_optimizer('main')

        # Progress the dataset iterator for bprop_len words at each iteration.
        batch = train_iter.__next__()
        
        xs_f = []
        ys_f = []
        xp = np
        if self.device >= 0:
                xp = chainer.backends.cuda.cupy
        for s in range(len(batch)):
            
            x = self.pickImages(dataPath + batch[s][0],xp)
            #x2 = Variable(self.min_max(self.pickArrays(dataPath + batch[s][0],xp),axis=0))
            #x2 = Variable(self.pickArrays(dataPath + batch[s][0],xp))
            #import ipdb;ipdb.set_trace()
            t = batch[s][1]
            #b_w = [ _b[2] for _b in b]
            #xs_f = xs_f.append(xs_f,x,axis=0)
            #xs_f.append([x,x2])
            xs_f.append(x)
            ys_f.append(int(t))
        #print(ys_f)
        ys_f = xp.array(ys_f).astype(xp.int32)
        #ys_f.append(xp.array([t]).astype(xp.int32))
        #print(ys_f)
        #import ipdb;ipdb.set_trace()
        loss = optimizer.target(xs_f,ys_f)
        #print(len(batch))
        #loss = loss / len(batch)
        #import ipdb;ipdb.set_trace()
        optimizer.target.cleargrads()  # Clear the parameter gradients
        loss.backward()  # Backprop
        loss.unchain_backward()  # Truncate the graph
        optimizer.update()  # Update the parameters

    def min_max(self,x, axis=None):
        #import ipdb;ipdb.set_trace()
        min = x.min(axis=axis, keepdims=True)
        max = x.max(axis=axis, keepdims=True)
        result = (x-min)/(max-min)
        return result

    def plusImages(self,image,num):
        return [image] * num

    def pickImages(self,path,xp):
        '''
        1発話分の画像を収集する
        '''
        
        #if os.path.exists("/mnt/data/shobayashi/cache/" + path.replace("/","_")): # pickleがあったら
        
        if os.path.exists(dirpath + path.replace("/","_")): # pickleがあったら
            
            #print("read",path.replace("/","_") + ".pickle")    
            #return 0
            #with open("cache/" + path.replace("/","_") + ".pickle",mode='rb') as f:
                #images = pickle.load("cache/" + path.replace("/","_") + ".pickle")
            images = joblib.load(dirpath + path.replace("/","_"))
            #import ipdb;ipdb.set_trace()
            #if len(images) % 6 != 0:
            #    images = np.append(images,self.plusImages(images[-1],6-len(images) % 6),axis=0)

        if xp != np:
            return chainer.backends.cuda.to_gpu(images)
        else:
            return images
    def pickArrays(self,path,xp):
        #import ipdb;ipdb.set_trace()
        if os.path.exists("cacheCSV/" + path.replace("/","_").replace("LF-ROI","CSV") + ".csv"): # pickleがあったら
            images = joblib.load("cacheCSV/" + path.replace("/","_").replace("LF-ROI","CSV") + ".csv")
        if xp != np:
            return chainer.backends.cuda.to_gpu(images)
        else:
            return images

def pickImages(path):
    '''
    1発話分の画像を収集する
    '''        
    #if os.path.exists("/mnt/data/shobayashi/cache/" + path.replace("/","_")): # pickleがあったら
    
    images = []
    for image_path in sorted(glob.glob(path + "/*"),key=numericalSort):
        #print(image_path)
        img = Image.open(image_path)
        img = img.convert('L')
        #img = img.resize((224,224), Image.ANTIALIAS)
        img = img.resize((80,80), Image.ANTIALIAS)
        #arrayImg = np.asarray(img).transpose(2,0,1).astype(np.float32)/255.
        arrayImg = np.asarray(img).astype(np.float32)/255.
        #import ipdb;ipdb.set_trace()
        images.append(arrayImg)
    images = np.asarray(images,dtype=np.float32)
    #with open("cache/" + path.replace("/","_") + ".pickle",mode='wb') as f:
    print("dump",path.replace("/","_") + ".pickle")
    #pickle.dump(images,"cache/" + path.replace("/","_") + ".pickle")
    joblib.dump(images,dirpath + path.replace("/","_"),compress=3)
    #import sys;sys.exit()


import re
def numericalSort(value):
    numbers = re.compile(r'(\d+)')
    parts = numbers.split(value)
    parts[1::2] = map(int, parts[1::2])
    return parts

def pickArrays(path):
    '''
    1発話分のランドマークを収集する
    '''
    #import ipdb;ipdb.set_trace()
    #if os.path.exists("/mnt/data/shobayashi/cache/" + path.replace("/","_")): # pickleがあったら
    #images = []
    #for image_path in glob.glob(path + "/*"):
    images = np.genfromtxt(path + ".csv",delimiter=",",dtype=np.float32)[:,:-1]
    #images.append(arrayImg)
    #images = images.astype(np.float32)
    #with open("cache/" + path.replace("/","_") + ".pickle",mode='wb') as f:
    print("dump",path.replace("/","_") + ".pickle")
    #pickle.dump(images,"cache/" + path.replace("/","_") + ".pickle")
    joblib.dump(images,dirpath + (path+".csv").replace("/","_"),compress=3)
    #import sys;sys.exit()
    
def makepickle(path):
    
    #import ipdb;ipdb.set_trace()
    #for path in paths:
    pickImages(dataPath + path[0])

def makepickleArrays(path):
    pickArrays(dataPath + path[0])

def read_dataset(path):
    with open(path,'r') as f:
        dataset = []
        for line in f:
            name, label = line.strip().split('\t')
            dataset.append([name,int(label)])
    return dataset


class lipEvaluator(extension.Extension):
    trigger = 1, 'epoch'
    default_name = 'validation'
    priority = extension.PRIORITY_WRITER
    def  __init__(self, iterator, target,
                 device=None, eval_hook=None, eval_func=None,out="result"):
        if isinstance(iterator, iterator_module.Iterator):
            iterator = {'main': iterator}
        self._iterators = iterator
        self.out = out
        
        if isinstance(target, link.Link):
            target = {'main': target}
        self._targets = target
        
        self.device = device
        self.eval_hook = eval_hook
        self.eval_func = eval_func

    def  get_iterator(self, name):
        """Returns the iterator of the given name."""
        return self._iterators[name]


    def  get_all_iterators(self):
        """Returns a dictionary of all iterators."""
        return dict(self._iterators)


    def  get_target(self, name):
        """Returns the target link of the given name."""
        return self._targets[name]


    def  get_all_targets(self):
        """Returns a dictionary of all target links."""
        return dict(self._targets)

    def __call__(self, trainer=None):
        # set up a reporter
        reporter = reporter_module.Reporter()
        if hasattr(self, 'name'):
            prefix = self.name + '/'
        else:
            prefix = ''
        for name, target in six.iteritems(self._targets):
            reporter.add_observer(prefix + name, target)
            reporter.add_observers(prefix + name,
                                   target.namedlinks(skipself=True))
        with reporter:
            with configuration.using_config('train', False):
                result = self.evaluate()

        reporter_module.report(result)
        return result

    def pickImages(self,path,xp):
        '''
        1発話分の画像を収集する
        '''
        
        #if os.path.exists("/mnt/data/shobayashi/cache/" + path.replace("/","_")): # pickleがあったら
        
        if os.path.exists(dirpath + path.replace("/","_")): # pickleがあったら
            
            #print("read",path.replace("/","_") + ".pickle")    
            #return 0
            #with open("cache/" + path.replace("/","_") + ".pickle",mode='rb') as f:
                #images = pickle.load("cache/" + path.replace("/","_") + ".pickle")
            images = joblib.load(dirpath + path.replace("/","_"))
            #import ipdb;ipdb.set_trace()
            #if len(images) % 6 != 0:
            #    images = np.append(images,self.plusImages(images[-1],6-len(images) % 6),axis=0)

        if xp != np:
            return chainer.backends.cuda.to_gpu(images)
        else:
            return images
    def pickArrays(self,path,xp):
        #import ipdb;ipdb.set_trace()
        if os.path.exists("cacheCSV/" + path.replace("/","_").replace("LF-ROI","CSV") + ".csv"): # pickleがあったら
            images = joblib.load("cacheCSV/" + path.replace("/","_").replace("LF-ROI","CSV") + ".csv")
        if xp != np:
            return chainer.backends.cuda.to_gpu(images)
        else:
            return images
        

    def evaluate(self):
        iterator = self._iterators['main']
        target = self._targets['main']
        eval_func = self.eval_func or target

        if self.eval_hook:
            self.eval_hook(self)
        it = copy.copy(iterator)
        summary = reporter_module.DictSummary()
        ans = [0] * 25
        acc = [0] * 25
        for batchs in it:
            #import ipdb;ipdb.set_trace()
            #print(batchs)
            observation = {}
            with reporter_module.report_scope(observation):
                #import ipdb;ipdb.set_trace()
                xs_f = []
                ys_f = []
                xp = np
                if self.device >= 0:
                    xp = chainer.backends.cuda.cupy
                for s in range(len(batchs)):
                    #print(batchs[s])
                    x = self.pickImages(dataPath + batchs[s][0],xp)
                    #x2 = Variable(self.min_max(self.pickArrays(dataPath + batchs[s][0],xp),axis=0))
                    #x2 = Variable(self.pickArrays(dataPath + batchs[s][0],xp))
                    t = batchs[s][1]
                    xs_f.append(x)
                    ys_f.append(int(t))
                ys_f = xp.array(ys_f).astype(xp.int32)
                eval_func(xs_f,ys_f)
                ans[int(eval_func.y.data.argmax())] += 1
                #import ipdb;ipdb.set_trace()
                if eval_func.y.data.argmax() == ys_f[0]:
                    acc[int(eval_func.y.data.argmax())] += 1


            summary.add(observation)
        print(ans)
        print(acc)
        return summary.compute_mean()

    def min_max(self,x, axis=None):
        #import ipdb;ipdb.set_trace()
        min = x.min(axis=axis, keepdims=True)
        max = x.max(axis=axis, keepdims=True)
        result = (x-min)/(max-min)
        return result

    

if __name__ == '__main__':
    main()
    pass
