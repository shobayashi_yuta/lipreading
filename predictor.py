############################
# 第５回サイレント音声認識ワークショップ「機械読唇チャレンジ」用プログラム
############################

import argparse
import glob
import os
import csv
import numpy as np
import copy
from PIL import Image
import six

import chainer
import chainer.functions as F
from chainer import function
import chainer.links as L
from chainer import link
from chainer.training import extensions
from chainer.training import extension
from chainer.dataset import iterator as iterator_module
from chainer import training
from chainer.links.caffe import CaffeFunction
from chainer import Variable, optimizers, Chain, cuda
from chainer.functions.evaluation import accuracy
from chainer.functions.loss import softmax_cross_entropy
from chainer import reporter as reporter_module
from chainer import configuration

import matplotlib
matplotlib.use('Agg')


import alexLike
#import compressed_pickle as pickle
import joblib
from multiprocessing import Pool

from main import RNN
#from main import Classifier
def main():
    
    parser = argparse.ArgumentParser()
    parser.add_argument('--corpus', '-c', type=str, default='/mnt/net/corpus/SSSD',
                        help='読み込むコーパス')
    parser.add_argument('--gpu', '-g', type=int, default=-1,
                        help='GPU ID (negative value indicates CPU)')
    parser.add_argument('--epoch', '-e', type=int, default=5,
                        help='Number of sweeps over the dataset to train')
    parser.add_argument('--out', '-o', default='result',
                        help='Directory to output the result')
    parser.add_argument('--save',action='store_true',
                        help='save tfrom chainer.functions.evaluation import accuracyraining model')
    args = parser.parse_args()
    data_type = 'test_CSV' # 画像を直接入れるかランドマークを利用するか
    global dataPath
    dataPath = args.corpus + "/" + data_type
    #import ipdb;ipdb.set_trace()
    #test = read_dataset(dataPath)
    test = read_dataset2()
    test_iter = chainer.iterators.SerialIterator(test, batch_size=1, repeat=False, shuffle=False)
    global xp
    xp = np
    rnn = RNN()
    model = Classifier(rnn)
    #chainer.serializers.load_npz("result/snapshot_epoch-1", model,path='updater/model:main/predictor/',strict=False)
    chainer.serializers.load_npz("result_copy/model_iter_53820", model)

    if args.gpu >= 0:
        # Make a specified GPU current
        xp = chainer.backends.cuda.cupy
        chainer.backends.cuda.get_device_from_id(args.gpu).use()
        model.to_gpu()
    evalutor(model,test_iter,len(test))


class Classifier(link.Chain):
    compute_accuracy = True

    def __init__(self, predictor,
                 lossfun=softmax_cross_entropy.softmax_cross_entropy,
                 accfun=accuracy.accuracy,
                 label_key=-1):
        if not (isinstance(label_key, (int, str))):
            raise TypeError('label_key must be int or str, but is %s' %
                            type(label_key))

        super(Classifier, self).__init__()
        self.lossfun = lossfun
        #self.lossfun = lossfun(class_weight=cupy.ndarray([1,2]).astype(cupy.int32))
        self.accfun = accfun
        self.y = None
        self.loss = None
        self.accuracy = None
        self.label_key = label_key

        with self.init_scope():
            self.predictor = predictor

    def __call__(self, *args, **kwargs):

        if isinstance(self.label_key, int):
            if not (-len(args) <= self.label_key < len(args)):
                msg = 'Label key %d is of bounds' % self.label_key
                raise ValueError(msg)
            t = args[self.label_key]
            if self.label_key == -1:
                args = args[:-1]
            else:
                args = args[:self.label_key] + args[self.label_key + 1:]
        elif isinstance(self.label_key, str):
            if self.label_key not in kwargs:
                msg = 'Label key "%s" is not found' % self.label_key
                raise ValueError(msg)
            t = kwargs[self.label_key]
            del kwargs[self.label_key]

        self.y = None
        self.loss = None
        self.accuracy = None
        
        self.y = self.predictor(*args, **kwargs)
        #import ipdb;ipdb.set_trace()
        #print(len(self.y),len(t))
        #print(len(self.y),len(t))
        #print(self.y[0])
        #sys.exit()
        #import ipdb;ipdb.set_trace()
        self.loss = self.lossfun(self.y, t)
        #ave = sum(self.predictor.length)/len(self.predictor.length)
        #ave = sum(self.predictor.length)
        #import ipdb;ipdb.set_trace()
        if self.compute_accuracy:
            self.accuracy = self.accfun(self.y, t)
            reporter_module.report({'accuracy': self.accuracy}, self)
        reporter_module.report({'loss': self.loss}, self)
        
        return self.loss


def evalutor(model,test_iter,num):
    for i in range(num):
        #import ipdb;ipdb.set_trace()
        batch = test_iter.next()
        images = chainer.backends.cuda.to_gpu(joblib.load(batch[0][0]))
        #images =  pickArrays(batch[0])
        with chainer.using_config('train', False), chainer.using_config('enable_backprop', False):
            eval_func = model.predictor([images])
        #print(str(i+1) + " / "  + str(num),eval_func.data.argmax(),batch[0].split("/")[-1])
        #result(batch[0].split("/")[-1],eval_func.data.argmax())
        print(str(i+1) + " / "  + str(num),eval_func.data.argmax(),batch[0][0].split("/")[-1])
        result2(batch[0][0].split("/")[-1],eval_func.data.argmax(),batch[0][1])

def result(path,res):
    with open('res.csv','a') as f:
        writer = csv.writer(f)
        writer.writerow([path,res])

def result2(path,res,ans):
    with open('resV.csv','a') as f:
        writer = csv.writer(f)
        writer.writerow([path,res,ans])


def pickArrays(path):
    '''
    1発話分のランドマークを収集する
    '''
    images = np.genfromtxt(path,delimiter=",",dtype=np.float32)[:,:-1]
    if xp != np:
        return chainer.backends.cuda.to_gpu(images)
    else:
        return images

import re
def numericalSort(value):
    numbers = re.compile(r'(\d+)')
    parts = numbers.split(value)
    parts[1::2] = map(int, parts[1::2])
    return parts

def read_dataset(path):
    return sorted(glob.glob(path+"/*/*"), key=numericalSort)
    #return glob.glob(path+"/*/*")


def read_dataset2():
    with open("validation_CSV.txt",'r') as f:
        dataset = []
        for line in f:
            name, label = line.strip().split('\t')
            dataset.append(["cacheCSV/.._.._ダウンロード_SSSD_CSV_" + name.replace("/","_"),label])
    return dataset

if __name__ == '__main__':
    main()
    pass
